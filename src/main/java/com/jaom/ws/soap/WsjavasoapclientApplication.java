package com.jaom.ws.soap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsjavasoapclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsjavasoapclientApplication.class, args);
	}

}
