package com.jaom.ws.soap;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.jaom.ws.trainings.CustomerOrdersPortType;
import com.jaom.ws.trainings.GetOrdersRequest;
import com.jaom.ws.trainings.GetOrdersResponse;
import com.jaom.ws.trainings.Order;
import com.jaom.ws.trainings.Product;

public class CustomerOrderWsClient {

    public static void main(String[] args) throws MalformedURLException {
        
        CustomerOrdersWsImplService service=
                new CustomerOrdersWsImplService(new URL("http://localhost:8080/wswsdlfirstws/customerorderservice?wsdl"));
        
        CustomerOrdersPortType customerOrdersWsImplPort=
                
                service.getCustomerOrdersWsImplPort();
        
        GetOrdersRequest request= new GetOrdersRequest();
        request.setCustomerId(BigInteger.valueOf(1));
        
        GetOrdersResponse response=
                customerOrdersWsImplPort.getOrders(request);
        
        List<Order> orders = response.getOrder();
        System.out.println("The number of orders for the customer are:"+
        		orders.size());
        
        
        Order firstOrder = orders.get(0);
        List<Product> products = firstOrder.getProduct();
        for(Product item : products){
    		System.out.println("Product ID: "+ item.getId());
    		System.out.println("Product Description: "+ item.getDescription());
    		System.out.println("Product quantity: "+ item.getQuantity());
    	}
        
        
        for(Order order : orders){
        	for(Product prod : order.getProduct()){
        		System.out.println("Product ID: "+ prod.getId());
        		System.out.println("Product Description: "+ prod.getDescription());
        		System.out.println("Product quantity: "+ prod.getQuantity());
        		
        	}
    	}
    }
}